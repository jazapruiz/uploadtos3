<?php
require_once 'vendor/autoload.php';

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

if (isset($_FILES["upload_file"])) {
    s3_upload_put_object($_FILES["upload_file"]);
}

function s3_upload_put_object($file)
{
    $options = [
        "region" => "us-east-2",
        'scheme' => 'http',
        "version" => "2006-03-01",
        "credentials" => [
            "key" => "YOUR_AWS_ACCESS_KEY",
            "secret" => "YOUR_AWS_SECRET_KEY",
        ]
    ];

    $file_name = $_FILES['upload_file']['name'];
    $tmp_file = $_FILES['upload_file']['tmp_name'];

    try {
        $s3Client = new S3Client($options);

        $result = $s3Client->putObject([
            "Bucket" => "YOUR_AWS_BUCKET",
            "Key" => $file_name,
            "SourceFile" => $tmp_file
        ]);

        if ($result->get("@metadata")["statusCode"] === 200) {
            echo "<div class=\"alert alert-success\" role=\"alert\">El archivo se subió correctamente</div>";
        }
    } catch (S3Exception $e) {
        echo "<div class=\"alert alert-danger\" role=\"alert\">" . $e->getMessage() . "</div>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <title>Subir archivos a S3 xD</title>
</head>

<body>
    <div class="container">
        <h2>Subir archivos a s3</h2>
        <br>
        <form method="POST" action="" enctype="multipart/form-data">
            <div class="mb-3">
                <label for="exampleInputFile" class="form-label">Archivo</label>
                <input type="file" name="upload_file" class="form-control" id="exampleInputFile" />
                <div id="emailHelp" class="form-text">Selecciona el archivo que desea subir.</div>
            </div>
            <input type='submit' class="btn btn-primary" name="upload_files" value='Subir' />
        </form>
    </div>
</body>